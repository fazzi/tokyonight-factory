#!/usr/bin/env bash

PICTURES_PATH=./out

if ! test -d "$PICTURES_PATH"; then
  mkdir -p "$PICTURES_PATH"
  echo "dir [./out] has been created successfully!"
fi

./conv.py "${@}"
