# 🗼Tokyo Night Factory 🏭

A simple cli to convert any image to a Tokyo Night themed wallpaper

![example1](./1.jpg)
![example2](./2.jpg)
Second is TokyoNight.

## Installation

1. Clone the repo.
2. Install the required packages using pip as shown below:
```
pip3 install -r requirements.txt
```
## Usage
from your terminal run:
```
./tokyonight.sh
```
or
```
./tokyonight.sh /path/to/image/
```

 All the outputs will be in ~/Pictures/Wallpapers/tokyonight


 ## Credits
- **Made** with [Schrodinger-Hat's ImageGoNord](https://github.com/Schrodinger-Hat), but with the Decay palette
- **Text User Interface (TUI)** made with [rich](https://github.com/willmcgugan/rich)
